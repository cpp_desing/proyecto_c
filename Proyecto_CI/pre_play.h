#ifndef PRE_PLAY_H
#define PRE_PLAY_H

#include <QWidget>
#include "mainwindow.h"

namespace Ui {
class pre_play;
}

class pre_play : public QWidget
{
    Q_OBJECT

public:
    explicit pre_play(QWidget *parent = nullptr);
    ~pre_play();

private slots:
    void on_play_now_clicked();

    void on_pushButton_clicked();

private:
    Ui::pre_play *ui;
    QMainWindow * game;
};

#endif // PRE_PLAY_H
