#include "missile.h"
#include <QTimer>
#include <QGraphicsScene>
#include <QDebug>
#include <math.h>



Missile::Missile(float vel_, float pos_, float k_,float masa_, float radio_,int *change_)
{
    change=change_;
    vel =vel_;
    pos = pos_;
    k = k_;
    masa = masa_;
    r = radio_;
    time = 1;
    ac = 0;
    Vec = 0;

    int random_number = rand() % 10;
   //position where the enemy appears
    if(random_number%2==0){
        move_left = true;
        setPos(1050,400);
       setPixmap(QPixmap(":/MRL.png"));
    }
    else
    {
    move_left=false;
    setPos(-50,400);
    setPixmap(QPixmap(":/MR.png"));

    }


    QTimer * timer_am = new QTimer();
    connect(timer_am,SIGNAL(timeout()),this,SLOT(actualizar_m()));
    timer_am->start(50);
}

float Missile::getvel()
{
    return vel;
}

float Missile::gettime()
{
    return time;
}

float Missile::getpos()
{
    return pos;
}

float Missile::getmasa()
{
    return masa;
}

float Missile::getradio()
{
    return r;
}

void Missile::set_vel(float vel_)
{
    vel = vel_;
}

void Missile::set_pos(float pos_)
{
    pos = pos_;
}

void Missile::stop_timer_am(bool x)
{
    if(x){
        timer_am->start(1410000000);

    }
    else{
        timer_am->start(50);

    }
}


void Missile::actualizar_m()
{
    QList<QGraphicsItem *> colliding_items =collidingItems();
    for (int i = 0, n = colliding_items.size();i<n;i++) {
        if(typeid(*(colliding_items[i])) == typeid(Character)){
            //scene()->removeItem(colliding_items[i]);
            *change-=20;
            scene()->removeItem(this);
            //delete colliding_items[i];
            delete this;
            return;
        }
    }

    set_vel(0.3);
    Vec = pow(((vel*vel)),1/2);
    ac = -((k*(Vec*Vec)*(r*r))/masa);
    pos = pos+(vel*time) +((ac*(time*time))/2);
    vel = vel + ac*time;
    if(move_left == true)
    {
        setPos(x()-pos/4,y());
        if(x() < 0)
        {
            scene()->removeItem(this);
            delete this;
        }

    }
    else
    {
        setPos(x()+pos/4,y());
        if(x() > 950)
        {
            scene()->removeItem(this);
            delete this;
        }
    }
}

