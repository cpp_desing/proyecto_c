#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsView>
#include "character.h"
#include <QTimer>
#include "enemy.h"
#include <QObject>
#include <mainwindow.h>
#include "menu.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    menu w;
    w.show();
    return a.exec();
}
