#ifndef CHARACTER_H
#define CHARACTER_H

#include <QObject>
#include <QGraphicsPixmapItem>
#include <QDebug>
#include "enemy.h"
#include "weapon.h"

class QTimer;

class Character: public QObject, public QGraphicsPixmapItem{
    Q_OBJECT
public:
    int *healt;
    int *score;
    int healt_;
    bool multiplayer;
    Character(float posX_, float posY_, float velX_,
              float velY_, float masa_, float radio_, float K_, float e_,int *p,bool multiplayer_);

    float& get_posX();
    float get_posY();
    float get_Radio();
    float get_e();
    float get_velX();
    float get_velY();
    float get_masa();
    void set_vel(float,float);

    void set_posX(float);
    void set_posY(float);

    void keyPressEvent(QKeyEvent * event);
    bool last_direction=false;
    friend inline QDebug operator<<(QDebug qd,const Character &p);

    void stop_all_timers(bool);

public slots:

    void spawn();
    void actualizar();

private:
    float PX;
    float PY;//posicion en y
    float mass;//masa del cuerpo
    float R;//radio del cuerpo
    float VX;//velocidad en x
    float VY;//velocidad en y
    float angulo;//angulo en el que va el cuerpo.
    float AX;//aceleracion en x
    float AY;//aceleracion en y
    float G;//gravedad
    float K;//resistencia del aire
    float e;//coeficiente de restitucion.
    float V;//vector velocidad.
    float dt;//delta de tiempo.

    Enemy * enemy;
    QTimer * timer_a;
    Weapon * weapon;



};
#endif // CHARACTER_H
