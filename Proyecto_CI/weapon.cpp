#include "weapon.h"
#include "enemy.h"
#include <QTimer>
#include <QGraphicsScene>
#include <QDebug>
#include <QList>
#include <QGraphicsItem>

void Weapon::stop_timer_bullet(bool x)
{
    if(x)timer_mbullet->start(141000000);
    else{timer_mbullet->start(20);}
}

Weapon::Weapon(bool _direction,int *p)
{
    direction = _direction;
    score=p;
    //Conecction
    timer_mbullet = new QTimer();
    connect(timer_mbullet,SIGNAL(timeout()),this,SLOT(move()));

    timer_mbullet->start(20);
}

void Weapon::move()
{
    QList<QGraphicsItem *> colliding_items =collidingItems();
    for (int i = 0, n = colliding_items.size();i<n;i++) {
        if(typeid(*(colliding_items[i])) == typeid(Enemy)){
            scene()->removeItem(colliding_items[i]);
            *score+=10;
            scene()->removeItem(this);
            delete colliding_items[i];
            delete this;
            return;
        }
    }
    if(direction==false){
        setPixmap(QPixmap(":/BR.png"));
        setPos(x()+25,y());
    }  // bullet direction
    if(direction==true){
        setPixmap(QPixmap(":/BL.png"));
        setPos(x()-25,y());
    }
    if(pos().x() < 0){
        scene()->removeItem(this);
        delete this;
    }
    else if(pos().x() > 1200){
        scene()->removeItem(this);
        delete this;
    }
}
