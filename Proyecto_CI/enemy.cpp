#include "enemy.h"
#include <QTimer>
#include <QGraphicsScene>
#include <QDebug>

Enemy::Enemy(float &pos_character)
{
    address_x=&pos_character;
    int random_number = rand() % 10;
   //position where the enemy appears
    if(random_number%2==0){
        move_left = true;
        setPos(1000,310);
        pos_x_enemy=1000;
    }
    else {
    move_left=false;
    setPos(-50,310);
    pos_x_enemy=-50;
       }

    // Connection
    timer_move_enemy = new QTimer();
    connect(timer_move_enemy,SIGNAL(timeout()),this,SLOT(move()));

    timer_move_enemy->start(50);
}

void Enemy::stop_timer_move_enemy(bool x)
{
    if(x) timer_move_enemy->start(1410000000);
    else{timer_move_enemy->start(50);}
}

void Enemy::move()
{
    if(*address_x<pos_x_enemy){
        setPixmap(QPixmap(":/ENEMY_L.png"));
        setPos(x()-5,y());
        pos_x_enemy-=5;
        }

    else{
        setPos(x()+5,y());
        setPixmap(QPixmap(":/ENEMY_R.png"));
        pos_x_enemy+=5;
        }

    if(pos().x() < -100){
        scene()->removeItem(this);
        delete this;
    }
    else if(pos().x() > 1100){
         scene()->removeItem(this);
         delete this;
    }
}
