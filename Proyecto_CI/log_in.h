#ifndef LOG_IN_H
#define LOG_IN_H

#include <QWidget>
#include "pre_play.h"
#include <QDebug>
#include <fstream>
#include <QString>
#include <cstring>
#include <QMessageBox>

using namespace std;

namespace Ui {
class Log_in;
}

class Log_in : public QWidget
{
    Q_OBJECT

public:
    explicit Log_in(QWidget *parent = nullptr);
    ~Log_in();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::Log_in *ui;
    pre_play * preplay;
};

#endif // LOG_IN_H
