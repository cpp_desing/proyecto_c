#ifndef WEAPON_H
#define WEAPON_H
#include <QGraphicsPixmapItem>
#include <QGraphicsItem>

class QTimer;

class Weapon: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
private:
    bool direction;

    int *score;
    QTimer * timer_mbullet;
public:

    Weapon(bool,int *p);
    void stop_timer_bullet(bool);

public slots:
    void move();
};

#endif // WEAPON_H
