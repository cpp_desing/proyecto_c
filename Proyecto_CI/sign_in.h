#ifndef SIGN_IN_H
#define SIGN_IN_H

#include <QWidget>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

namespace Ui {
class Sign_in;
}

class Sign_in : public QWidget
{
    Q_OBJECT

public:
    explicit Sign_in(QWidget *parent = nullptr);
    ~Sign_in();

private slots:

    void on_back_clicked();

    void on_accept_clicked();

private:
    Ui::Sign_in *ui;
};

#endif // SIGN_IN_H
