#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtCore>
#include <QGraphicsRectItem>
#include <QDialog>
#include <QMessageBox>

MainWindow::MainWindow(bool multiplayer_,QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    multiplayer=multiplayer_;

    score=&score_;
    score_2=&score2;
    ui->setupUi(this);
    scene = new QGraphicsScene(0,0,1000,450);  //tamaño de mi escena
    ui->graphicsView->setScene(scene);  //monto la escena al mainwindow
    ui->graphicsView->adjustSize();  //se acomodan ls dimenciones
    ui->graphicsView->setBackgroundBrush(QBrush(QImage(":/BG")));
    player = new Character(500,200,0,0,50,25,0,0,score,false);
    if (multiplayer){
        player2 = new Character(300,200,0,0,50,25,0,0,score_2,true);
        //ui->healt->display(player->healt_);
        player2->setPixmap(QPixmap(":/BW_R.png"));
        player2->setFlag(QGraphicsPixmapItem::ItemIsFocusable);
        player2->setFocus();

        scene->addItem(player2);
        player2->setPos(500,200);
    }
    ui->healt->display(player->healt_);
    player->setPixmap(QPixmap(":/JW_R.png"));
    player->setFlag(QGraphicsPixmapItem::ItemIsFocusable);
    player->setFocus();

    scene->addItem(player);  //añando el jugador

    timer_spawn = new QTimer();  //timer de enemigos
    QObject::connect(timer_spawn,SIGNAL(timeout()),player,SLOT(spawn()));
    timer_spawn->start(2000);

    timer_spawno = new QTimer();  //timer de misile
    QObject::connect(timer_spawno,SIGNAL(timeout()),this,SLOT(misilile()));
    timer_spawno->start(2000);

    actu = new QTimer();  //timer de misile
    QObject::connect(actu,SIGNAL(timeout()),this,SLOT(actualize_displayers()));
    actu->start(100);

    player->setPos(500,200); //lugar de inicio de mi personaje.
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::stop_timers_w(bool x)
{
    if(x){
        timer_spawn->start(141000000);
        player->stop_all_timers(true);
    }
    else{
        timer_spawn->start(2000);
        player->stop_all_timers(false);
    }

}

void MainWindow::actualize_displayers()
{
    ui->num_score->display(score_);
    ui->num_score2->display(score2);
    ui->healt->display(player->healt_);


}

void MainWindow::on_pushButton_clicked()
{
    stop_timers_w(true);
    QMessageBox::StandardButton game_over= QMessageBox::question(this,"Pausa","Juego pausado",
                     QMessageBox::Retry | QMessageBox::Close);
    if(game_over==QMessageBox::Retry){
        //save game
        this->close();
    }
    if(game_over==QMessageBox::Close){
        stop_timers_w(false);
    }
}

void MainWindow::misilile()
{
    misil = new Missile(0,-50,0,50,25,player->healt);
    scene->addItem(misil);
    ui->num_score->display(score_);
    ui->num_score2->display(score2);
    ui->healt->display(player->healt_);
    if(*misil->change<=0)
    {
        *misil->change = 100;
        this->close();
        QMessageBox::question(this,"Game Over","Has perdido", QMessageBox::Ok);
        //QApplication::quit();
        //save game
    }

}
