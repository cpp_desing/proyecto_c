#ifndef MENU_H
#define MENU_H

#include <QWidget>
#include <QPainter>
#include "log_in.h"
#include "sign_in.h"

namespace Ui {
class menu;
}

class menu : public QMainWindow
{
    Q_OBJECT

public:
    explicit menu(QWidget *parent = nullptr);
    ~menu();


private slots:

    void on_log_in_button_clicked();

    void on_sign_in_button_clicked();

private:

    Ui::menu *ui;
    Sign_in * sigin;
    Log_in * login;


};

#endif // MENU_H
