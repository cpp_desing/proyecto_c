#include "pre_play.h"
#include "ui_pre_play.h"

pre_play::pre_play(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::pre_play)
{
    ui->setupUi(this);
}

pre_play::~pre_play()
{
    delete ui;
}

void pre_play::on_play_now_clicked()
{
    game = new MainWindow(false,this);
    game->show();
}

void pre_play::on_pushButton_clicked()
{
    game = new MainWindow(true,this);
    game->show();
}
