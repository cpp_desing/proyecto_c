#ifndef ENEMY_H
#define ENEMY_H

#include <QGraphicsPixmapItem>
#include <QObject>

class QTimer;

class Enemy: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Enemy(float &pos_character);
    bool move_left;

    float *address_x;
    void stop_timer_move_enemy(bool);

private:
    int pos_x_enemy;

    QTimer * timer_move_enemy;
public slots:
    void move();
};


#endif // ENEMY_H
