#include "sign_in.h"
#include "ui_sign_in.h"
#include <QDebug>


Sign_in::Sign_in(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Sign_in)
{
    ui->setupUi(this);
}

Sign_in::~Sign_in()
{
    delete ui;
}


void Sign_in::on_back_clicked()
{
    //confirm and save data of new user.
    ui->line_email->clear();
    ui->line_pass->clear();
    Sign_in(this).close();
    this->close();
}

void Sign_in::on_accept_clicked()
{
    QString email=ui->line_email->text();
    QString pass=ui->line_pass->text();
    qDebug()<<pass;
    qDebug()<<email;
    string n_archivo = "archivo.txt";
    ofstream archivo (n_archivo,ios::app);
    email+="-";email+=pass;
    qDebug()<<email;
    string data=email.toStdString();
    archivo<<data<<endl;
    ui->line_email->clear();
    ui->line_pass->clear();
    archivo.close();

}
