#ifndef MISSILE_H
#define MISSILE_H


#include <QGraphicsPixmapItem>
#include <QGraphicsItem>
#include <QObject>
#include "character.h"

class QTimer;

class Missile: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    int *change;
    Missile(float vel_, float pos_, float k_,float masa_, float radio_,int *change_);
    float getvel();
    float gettime();
    float getpos();
    float getmasa();
    float getradio();
    bool move_left;
    void set_vel(float);
    void set_pos(float);

    void stop_timer_am(bool);

private:
    int pos_x_misil;
    float vel;
    float time;
    float pos;
    float ac;
    float k;
    float Vec;
    float masa;
    float angulo;
    float r;

    QTimer * timer_am;


public slots:

    void actualizar_m();
};

#endif // MISSILE_H
