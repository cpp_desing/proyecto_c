#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMessageBox>
#include <QMainWindow>
#include <QGraphicsScene>
#include "character.h"
#include <QTimer>
#include <QGraphicsView>
#include <QMessageBox>
#include <QGraphicsLineItem>
#include "missile.h"
#include <QList>
#include <QGraphicsItem>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(bool multiplayer,QWidget *parent = nullptr);
    ~MainWindow();
    bool multiplayer;
    void stop_timers_w(bool);
    void actualize_displayers();

private slots:

    void on_pushButton_clicked();

public slots:
    void misilile();


private:
    int *score;
    int score_=0;
    int score2=0;
    int *score_2;
    Ui::MainWindow *ui;
    QGraphicsScene * scene;
    Character * player;
    Character * player2;
    Missile * misil;

    QTimer * timer_spawn;
    QTimer * timer_spawno;
    QTimer * actu;



};

#endif // MAINWINDOW_H
