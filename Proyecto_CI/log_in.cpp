#include "log_in.h"
#include "ui_log_in.h"

#include <iostream>

using namespace std;


Log_in::Log_in(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Log_in)
{
    ui->setupUi(this);
}

Log_in::~Log_in()
{
    delete ui;
}


void Log_in::on_buttonBox_accepted()
{
    QString email=ui->line_mail->text();
    QString pass=ui->line_pas->text();
    email+="-";email+=pass;
    qDebug()<<pass;
    qDebug()<<email;
    ifstream datal;
    datal.open("archivo.txt");
    string user;
    if(datal.fail()){
        qDebug()<<"Error: Archivo no encontrado.";
    }
    else{
        while (!datal.eof()) {
            getline(datal,user);
            string validar = email.toStdString();
            if(user==validar){
                preplay = new pre_play(this);
                preplay->show();
                datal.close();
                break;
            }
        }
  }
    ui->line_mail->clear();
    ui->line_pas->clear();
}

void Log_in::on_buttonBox_rejected()
{
    Log_in(this).close();
    this->close();
}
