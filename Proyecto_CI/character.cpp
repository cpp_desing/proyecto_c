#include "character.h"
#include "enemy.h"
#include <QKeyEvent>
#include <QGraphicsScene>
#include <math.h>
#include <cmath>
#include <QDebug>
#include "missile.h"
#include <QTimer>

void Character::keyPressEvent(QKeyEvent *event)
{
    //QSet<int> pressedKeys;

    if(!multiplayer){
        if(event->key() == Qt::Key_Left){

            setPixmap(QPixmap(":/JW_L.png"));

            if(pos().x() > 0 )
            {
            setPos(x()-10,y());
            set_posX(-10);
            }
            last_direction=true;
        }
        else if (event->key() == Qt::Key_Right) {

            setPixmap(QPixmap(":/JW_R.png"));

            if (pos().x() + 50 < 1000)
            {
            setPos(x()+10,y());
            set_posX(10);
            }
            last_direction=false;
        }

        else if (event->key()== Qt::Key_Space) {

            weapon = new Weapon(last_direction,score);
            if(last_direction==true) weapon->setPos(x()+2,y()+55);  //Location of the bullet at the beginning.
            else weapon->setPos(x()+55,y()+55);
            scene()->addItem(weapon);
        }
        else if (event->key() == Qt::Key_Up && get_posY()<=30) {

            if(last_direction) setPixmap(QPixmap(":/JW_JUMP_L.png"));
            else setPixmap(QPixmap(":/JW_JUMP_R.png"));

            if(last_direction==true){
                set_vel(-10,50);
            }
            else if(last_direction==false){
                set_vel(10,50);
            }
        }
    }
    else {
        if(event->key() == Qt::Key_A){

            setPixmap(QPixmap(":/BW_L.png"));

            if(pos().x() > 0 )
            {
            setPos(x()-10,y());
            set_posX(-10);
            }
            last_direction=true;
        }
        else if (event->key() == Qt::Key_D) {

            setPixmap(QPixmap(":/BW_R.png"));

            if (pos().x() + 50 < 1000)
            {
            setPos(x()+10,y());
            set_posX(10);
            }
            last_direction=false;
        }

        else if (event->key()== Qt::Key_E) {

            weapon = new Weapon(last_direction,score);
            if(last_direction==true) weapon->setPos(x()+2,y()+55);  //Location of the bullet at the beginning.
            else weapon->setPos(x()+55,y()+55);
            scene()->addItem(weapon);
        }
        else if (event->key() == Qt::Key_W && get_posY()<=30) {

            if(last_direction) setPixmap(QPixmap(":/BW_JUMP_L.png"));
            else setPixmap(QPixmap(":/BW_JUMP_R.png"));

            if(last_direction==true){
                set_vel(-10,50);
            }
            else if(last_direction==false){
                set_vel(10,50);
            }
        }
    }

}







void Character::stop_all_timers(bool x)
{
    if(x){
        timer_a->start(1410000000);
        enemy->stop_timer_move_enemy(x);
        //misil->stop_timer_am(x);
        weapon->stop_timer_bullet(x);
    }
    else{
        timer_a->start(50);
        enemy->stop_timer_move_enemy(x);
        //misil->stop_timer_am(x);

    }
}


void Character::spawn()
{
    enemy = new Enemy(get_posX());
    scene()->addItem(enemy);
}

Character::Character(float posX_, float posY_,
                     float velX_, float velY_, float masa_, float radio_, float K_, float e_,int *p,bool multiplayer_)
{
    multiplayer=multiplayer_;
    score=p;
    healt_=100;
    healt=&healt_;

    PX = posX_;
    PY = posY_;
    mass = masa_;
    R = radio_;
    VX = velX_;
    VY = velY_;
    AX = 0;
    AY = 0;
    G = 10;
    K = K_;
    e = e_;
    V = 0;
    dt = 1;

    timer_a = new QTimer();
    connect(timer_a,SIGNAL(timeout()),this,SLOT(actualizar()));
    timer_a->start(50);
}


float& Character::get_posX()
{
    return PX;
}

float Character::get_posY(){//retorna la posicion en y.
    return PY;
}

float Character::get_Radio(){//retorna el radio
    return R;
}

float Character::get_velX(){//retorna la velocidad en x.
    return VX;
}

float Character::get_velY(){//retorna la velocidad en y.
    return VY;
}

float Character::get_masa(){//retorna la masa
    return mass;
}


float Character::get_e(){//retorna el coeficiente de restitucion.
    return e;
}

void Character::set_vel(float velx, float vely)
{
    VX = velx;
    VY = vely;
}



void Character::actualizar()
{
    if(VX<10)angulo = 90;
    else { angulo = atan2(VY,VX);}
    V = pow(((VX*VX)+(VY*VY)),1/2);
    AX = -((K*(V*V)*(R*R))/mass)*cos(angulo);
    AY = (-((K*(V*V)*(R*R))/mass)*sin(angulo))-G;
    PX = PX+(VX*dt) +((AX*(dt*dt))/2);
    PY = PY + (VY*dt) +((AY*(dt*dt))/2);
    VX = VX + AX*dt;
    VY = VY + AY*dt;

    if(PY>350 || PY<30){
        VY=0;
        PY=30;
        VX=0;
    }
    if(PX>950)
    {
        PX=950;
    }
    else if(PX<0)
    {
        PX = 0;
    }

    setPos(PX,340-PY);
   // qDebug()<<get_posY();
}


void Character::set_posX(float pos)
{
    PX+=pos;
}

void Character::set_posY(float pos)
{
    PY+=pos;
}
